# README #

This is a boilerplate code cum directory structure that we will be following for further projects.

### What is this repository for? ###

* This repo provides a quick startup directory structure to get started with your projects
* It comes with redux and thunk pre installed
* PWA is not supported yet, but future releases will have a provision to create PWA as well
* Version : v1.0

### How do I get set up? ###

* Take clone using ```git clone https://h4rdik_chn@bitbucket.org/h4rdik_chn/react-csr-boilerplate.git```
* Run the following commands to remove admin's origin and add yours
    ```git remote remove origin```
    ```git remote add origin [your git repo]```
    ```npm i / yarn```
    ```happy hacking !```

### Contribution guidelines ###

* To contribute please run the steps above and create a pull request for admin's dev branch

### Who do I talk to? ###

* Hardik Chauhan (hardik.chauhan@affle.com)
* Vatandeep Singh