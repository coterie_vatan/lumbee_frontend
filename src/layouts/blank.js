import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import Header from "../components/common/header";
import { connect } from "react-redux";
const HomeLayout = props => {
  return (
    <div className="main_wrapper">
      <Header />
      {props.children}
    </div>
  );
};

const HomeLayoutComponent = connect()(HomeLayout);

const HomeLayoutRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <HomeLayoutComponent>
          <Component {...matchProps} />
        </HomeLayoutComponent>
      )}
    />
  );
};

export default HomeLayoutRoute;
