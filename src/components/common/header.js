import React, { Component } from "react";
import { Link } from "react-router-dom";
export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <header className="lumbee-header">
        <a href="">
          <img src="https://static.wixstatic.com/media/acf11c_3d7d7ce9836e4ff5b1caf542c59972ba~mv2.png/v1/fill/w_311,h_64,al_c,q_85,usm_0.66_1.00_0.01/logo.webp" />
        </a>
        <div className="lumbee-navigation">
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/">About us</Link>
            </li>
            <li>
              <Link to="/">Teachers</Link>
            </li>
            <li>
              <Link to="/">Cirriculum</Link>
            </li>
            <li>
              <Link to="/">Parents</Link>
            </li>
            <li>
              <Link to="/">Register</Link>
            </li>
          </ul>
        </div>
      </header>
    );
  }
}
