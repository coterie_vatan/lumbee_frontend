import React from "react";
import "../../assets/css/App.css";

function App() {
  return (
    <div className="lumbee">
      <div className="main_slogan">
        <h2>
          “Online Learning is not the next big thing, it is now the big thing.”
        </h2>
        <h3>Donna J Abbernathy</h3>
      </div>
      <div className="banner">
        <img src="https://static.wixstatic.com/media/c22c23_a8f1b749824e4a46ac88b3f8ae14b95f~mv2.jpg/v1/fill/w_1280,h_489,al_c,q_85,usm_0.66_1.00_0.01/c22c23_a8f1b749824e4a46ac88b3f8ae14b95f~mv2.webp" />
      </div>
      <div className="about_us">
        <div className="inner_about">
          <h1>LumBee Learning</h1>
          <h2>Rethink Education</h2>
          <p>
            Active learning is not a theory but a teaching method that supports
            learning. Active learning" means students engage with the material,
            participate in the class, and collaborate. pre-COVID tools were
            handouts, whiteboard, chalkboard, and post-COVID, they are
            smartphone apps, platforms such as Google Drive or Twitter.{" "}
          </p>

          <p>
            ​ Another way to define active learning is to consider the
            opposite—passive learning where students are recipients of
            knowledge, are expected to record and absorb knowledge delivered by
            an expert—a faculty member or textbook.
          </p>
        </div>
      </div>
      <div className="services">
        <h2>Considering homeschooling for your child? </h2>
        <h2> See why LumBee Learning is the right choice for your child.</h2>
        <div className="common_inner">
          <div className="item">
            <img src="https://static.wixstatic.com/media/11062b_d5843c6c703b418b91307ec0d59834f2~mv2.jpeg/v1/fill/w_314,h_208,al_c,q_80,usm_0.66_1.00_0.01/Girl%20with%20Laptop.webp" />
            <div className="right_content">
              <h3>HIGHER FLEXIBILITY</h3>​
              <p>
                Online courses are guided through a structured and flexible
                procedure according to your needs.
              </p>
            </div>
          </div>
          <div className="item right">
            <img src="https://static.wixstatic.com/media/11062b_82018cea0f894d82b12d57b3a0ae0b70~mv2.jpg/v1/fill/w_330,h_208,al_c,q_80,usm_0.66_1.00_0.01/Jar%20of%20Coins.webp" />
            <div className="right_content">
              <h3>LOWER COSTS</h3>​
              <p>
                ​ We offer cost-effective and affordable courses that are viable
                in comparison to attending traditional schools.
              </p>
            </div>
          </div>
          <div className="item">
            <img src="https://static.wixstatic.com/media/11062b_64d05b10dd3943f9bc6db8866cce5b4d~mv2.jpeg/v1/fill/w_316,h_201,al_c,q_80,usm_0.66_1.00_0.01/Distance%20Learning.webp" />
            <div className="right_content">
              <h3>PERSONALIZED LEARNING</h3>​
              <p>
                Personalized learning targets your weak areas and helps you
                overcome them to succeed at a faster pace.
              </p>
            </div>
          </div>
          <div className="item right">
            <img src="https://static.wixstatic.com/media/11062b_8254605b489d41a29829b728740119d7~mv2.jpg/v1/fill/w_314,h_208,al_c,q_80,usm_0.66_1.00_0.01/Distance%20Learning.webp" />
            <div className="right_content">
              <h3>COMPREHENSIVE CURRICULUM</h3>​
              <p>
                Wide range of courses available from different curriculum from
                around the world to give you a vast exposure.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
